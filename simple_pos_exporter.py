#!/usr/bin/python3

# This is a python file
# -*- coding: utf-8 -*-

import pcbnew
import logging
import csv
import os
import argparse

logger = logging.getLogger(__name__)

PCBNEW_PTH_ATTRIBUTE = 0
PCBNEW_SMD_ATTRIBUTE = 1

def get_footprint_pnp_details(board, include_top=True, include_btm=True, include_PTH=False):
    details = []
    for module in board.GetModules():
        if (module.IsFlipped() and not include_btm) or (not module.IsFlipped() and not include_top):
            continue

        if not (module.GetAttributes() == PCBNEW_SMD_ATTRIBUTE or (module.GetAttributes() == PCBNEW_PTH_ATTRIBUTE and include_PTH)):
            logger.debug(f"ignoring component: {module.GetReference()}")
            continue
        
        logger.debug(f"including component: {module.GetReference()}")
        wx_mid_point = module.GetCenter()
        mid_point = (pcbnew.ToMM(wx_mid_point[0]), pcbnew.ToMM(wx_mid_point[1]))

        mod_detail = {
            'Mid X': mid_point[0],
            'Mid Y': mid_point[1],
            'Designator': module.GetReference(),
            'Val': module.GetValue(),
            'Rotation': module.GetOrientationDegrees(),
            'Package': str(module.GetFPID().GetLibItemName()),
            'Layer': 'bottom' if module.IsFlipped() else 'top'
        }
        details.append(mod_detail)
    return details


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Command-line 3D image export from pcbnew using xdotool and emulating a GUI interface')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='show debugging information')
    parser.add_argument('-b', '--brd-file', required=True,
                        help='The KiCad/pcbnew .kicad_pcb file')
    parser.add_argument('-p', '--include-pth', action='store_true',
                        help='include through-hole components in the pos file')
    parser.add_argument('-t', '--output-top-pos', required=True,
                        help='The destination csv filename for pnp file for top side')
    parser.add_argument('-m', '--output-btm-pos', required=True,
                        help='The destination csv filename for pnp file for btm side')

    args = parser.parse_args()

    LOG_LEVEL = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=LOG_LEVEL)

    if not os.path.isfile(args.brd_file):
        logger.error(f"PCB file not found: {args.brd_file}")

    board = pcbnew.LoadBoard(args.brd_file)

    details_top = get_footprint_pnp_details(board, include_btm=False, include_PTH=args.include_pth)
    details_btm = get_footprint_pnp_details(board, include_top=False, include_PTH=args.include_pth)
    params_list = [
        'Designator',
        'Val',
        'Package',
        'Mid X',
        'Mid Y',
        'Rotation',
        'Layer'
    ]

    if len(details_top) > 0:
        with open(args.output_top_pos, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow(params_list)
            for mod_detail in details_top:
                row = []
                for param in params_list:
                    row.append(mod_detail[param])
                writer.writerow(row)
    
    if len(details_btm) > 0:
        with open(args.output_btm_pos, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow(params_list)
            for mod_detail in details_btm:
                row = []
                for param in params_list:
                    row.append(mod_detail[param])
                writer.writerow(row)